<?php 
/*	
*	---------------------------------------------------------------------
*	MUSEE DES ORMONTS Functions
*	--------------------------------------------------------------------- 
*/

add_image_size( '400x1190-cropped', 1190, 400, true );


/*
 * Enqueue parent (Quince) style
 */
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

// Our custom post type function
function create_exposition_post_type() {
    register_post_type( 'exposition',
        array(
            'labels' => array(
                'name' => __( 'Expositions' ),
                'singular_name' => __( 'Exposition' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}
add_action( 'init', 'create_exposition_post_type' );

function get_translation($source='') {

    require_once 'language.php';

    $result = [];
    $request = explode('.', $source);
    $translations = get_traductions();

    foreach ($request as $key => $value) {
        if(!isset($traduction_result)) {
            $traduction_result = $translations[ICL_LANGUAGE_CODE][$value];
        }else{
            $traduction_result = $traduction_result[$value];
        }
    }

    echo $traduction_result;
}

function switcher_lang(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        echo '<div id="language_list"><ul>';
        foreach($languages as $l){
            echo '<li>';
            if($l['country_flag_url']){
                if(!$l['active']) echo '<a href="'.$l['url'].'">';
                if(!$l['active']) echo '</a>';
            }
            if(!$l['active']) echo '<a href="'.$l['url'].'">';
            echo icl_disp_language($l['language_code']);
            if(!$l['active']) echo '</a>';
            echo '</li>';
        }
        echo '</ul></div>';
    }
}
add_shortcode('switcher_lang', 'switcher_lang');

function getFileSizeFromUrl($file){

    $ch = curl_init($file);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, TRUE);
    curl_setopt($ch, CURLOPT_NOBODY, TRUE);

    $data = curl_exec($ch);
    $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

    curl_close($ch);

    return FileSizeConvert($size);

}

/*
 * Converts bytes into human readable file size.
 *
 * @param string $bytes
 * @return string human readable file size (2,87 Мб)
 * @author Mogilev Arseny
*/
function FileSizeConvert($bytes)
{
    $bytes = floatval($bytes);
    $arBytes = array(
        0 => array(
            "UNIT" => "TB",
            "VALUE" => pow(1024, 4)
        ),
        1 => array(
            "UNIT" => "GB",
            "VALUE" => pow(1024, 3)
        ),
        2 => array(
            "UNIT" => "MB",
            "VALUE" => pow(1024, 2)
        ),
        3 => array(
            "UNIT" => "KB",
            "VALUE" => 1024
        ),
        4 => array(
            "UNIT" => "B",
            "VALUE" => 1
        ),
    );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}


