<?php
/**
 * Template name: Homepage
 */

$slider = get_field('slider',$post->ID);
$elements = get_field('elements',$post->ID);
$form_mailchimp = get_field('form_mailchimp',$post->ID);

get_header('home'); ?>


        <section class="home-expositions">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php foreach($slider as $slide): ?>
                        <div class="swiper-slide" style="background-image: url('<?php echo $slide['image']; ?>')"></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

		<div id="container" class="row-inner">
			<div id="content">

				<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
					<div class="entry-content clearfix">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
				</article>

				<?php if (ot_get_option('page_comments') != 'off') {
					echo '<div class="row-inner"><div class="vc_span12 wpb_column column_container">';
						comments_template( '', true );
					echo '</div></div>';
				} ?>
				<?php endwhile; ?>

                <div class="CTA-homepage">
                    <?php foreach($elements as $element): ?>
                        <article>
                            <div class="image" style="background-image: url('<?php echo $element['image']; ?>')">
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                            </div>
                            <div>
                                <h2><?php echo $element['title']; ?></h2>
                                <p>
                                    <?php echo $element['content']; ?>
                                </p>
                                <a href="<?php echo $element['url']; ?>" title="<?php echo $element['title']; ?>">
                                    <?php get_translation('info_more') ?>
                                </a>
                            </div>
                        </article>
                    <?php endforeach; ?>
                </div>

                <div class="newsletter">
                    <div class="txt">
                        <?php get_translation('singup_newsletter') ?>
                    </div>
                    <div class="form">
                        <?php echo do_shortcode($form_mailchimp); ?>
                    </div>
                </div>

			</div><!-- #content -->
		</div><!-- #container -->
		
<?php get_footer('home'); ?>