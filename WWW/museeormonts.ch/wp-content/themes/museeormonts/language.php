<?php
function get_traductions() {
    return [
        'fr' => [
            'singup_newsletter' => 'Inscription à la newsletter',
            'info_more' => 'Plus d\'info',
            'read_more' => 'Lire plus',
            'download' => 'Téléchargement',
            'sponsors' => 'Sponsors',
            'expositions' => [
                'other_expo' => 'A voir aussi',
                'gallery' => 'Galerie d\'images',
                'contest' => 'Concours',
                'current_expo' => 'Expositions actuelles',
                'next_expo' => 'Prochaines expositions',
                'archive_expo' => 'Archives',
                '1_price' => '1<sup>er</sup> prix du jury',
                '2_price' => '2<sup>ème</sup> prix du jury',
                '3_price' => '3<sup>ème</sup> prix du jury',
            ],
            'blog' => [
                'title' => 'Actualités'
            ]
        ],
        'de' => [
            'info_more' => 'Plus d\'info',
            'read_more' => 'Lire plus',
            'download' => 'Téléchargement',
            'sponsors' => 'Sponsors',
            'expositions' => [
                'other_expo' => 'A voir aussi',
                'gallery' => 'Galerie d\'images',
                'contest' => 'Concours',
                'current_expo' => 'Expositions actuelles',
                'next_expo' => 'Prochaines expositions',
                'archive_expo' => 'Archives',
            ]
        ],
        'en' => [
            'info_more' => 'Plus d\'info',
            'read_more' => 'Lire plus',
            'download' => 'Téléchargement',
            'sponsors' => 'Sponsors',
            'expositions' => [
                'other_expo' => 'A voir aussi',
                'gallery' => 'Galerie d\'images',
                'contest' => 'Concours',
                'current_expo' => 'Expositions actuelles',
                'next_expo' => 'Prochaines expositions',
                'archive_expo' => 'Archives',
            ]
        ],
    ];
}