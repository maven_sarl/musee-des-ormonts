<?php
/**
 * Template name: Expositions
 */

$args = array(
    'post_type'  => 'exposition',
    'meta_key'			=> 'start_date_exposition',
	'orderby'			=> 'meta_value',
	'order'				=> 'DESC'
);

$now = [];
$next = [];
$archives = [];

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

$expositions = get_posts( $args );

get_header('no-title'); ?>

    <?php if($featured_img_url): ?>
        <div class="banner-post" style="background-image: url(<?php echo $featured_img_url ?>)"></div>
    <?php endif; ?>

    <div id="container" class="row-inner">

            <div id="content">

                <?php

                foreach ($expositions as $exposition){

                    $status_exposition = get_field('status_exposition', $exposition->ID);

                    if($status_exposition == 'in_progress') {
                        $now[] = $exposition;
                    }

                    if($status_exposition == 'next') {
                        $next[] = $exposition;
                    }

                    if($status_exposition == 'archive') {
                        $archives[] = $exposition;
                    }

                }

                ?>

                <?php if($now): ?>
                    <h1><?php get_translation('expositions.current_expo') ?></h1>
                    <?php foreach($now as $item): ?>
                        <?php
                        $poster = get_field('poster', $item->ID);
                        $start_date = get_field('start_date_exposition', $item->ID);
                        $end_date = get_field('end_date_exposition', $item->ID);
                        $show_day = get_field('show_day', $item->ID);
                        ?>
                        <article class="exposition">
                            <figure>
                                <img src="<?php echo $poster; ?>" alt="<?php echo $item->post_title; ?>">
                            </figure>
                            <div>
                                <h2><?php echo $item->post_title; ?></h2>
                                <span class="date">
                                    <?php

                                    if($show_day) {
                                        echo $start_date . ' - ' . $end_date;
                                    }else{
                                        echo substr($start_date, 2) . ' - ' . substr($end_date, 2);
                                    }

                                    ?>
                                </span>
                                <?php echo $item->post_content; ?>
                                <a href="<?php echo $item->guid; ?>" class="more-exposition">
                                    <?php get_translation('read_more') ?>
                                </a>
                            </div>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if($next): ?>
                    <h1 class="expo"><?php get_translation('expositions.next_expo') ?></h1>
                    <?php foreach($next as $item): ?>
                        <?php
                        $poster = get_field('poster', $item->ID);
                        $start_date = get_field('start_date_exposition', $item->ID);
                        $end_date = get_field('end_date_exposition', $item->ID);
                        $show_day = get_field('show_day', $item->ID);
                        ?>
                        <article class="exposition">
                            <figure>
                                <img src="<?php echo $poster; ?>" alt="<?php echo $item->post_title; ?>">
                            </figure>
                            <div>
                                <h2><?php echo $item->post_title; ?></h2>
                                <span class="date">
                                    <?php

                                    if($show_day) {
                                        echo $start_date . ' - ' . $end_date;
                                    }else{
                                        echo substr($start_date, 2) . ' - ' . substr($end_date, 2);
                                    }

                                    ?>
                                </span>
                                <?php echo $item->post_content; ?>
                                <a href="<?php echo $item->guid; ?>" class="more-exposition">
                                    <?php get_translation('read_more') ?>
                                </a>
                            </div>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if($archives): ?>
                    <h1 class="expo"><?php get_translation('expositions.archive_expo') ?></h1>
                    <?php foreach($archives as $item): ?>
                        <?php
                        $poster = get_field('poster', $item->ID);
                        $start_date = get_field('start_date_exposition', $item->ID);
                        $end_date = get_field('end_date_exposition', $item->ID);
                        $show_day = get_field('show_day', $item->ID);
                        ?>
                        <article class="exposition">
                            <figure>
                                <img src="<?php echo $poster; ?>" alt="<?php echo $item->post_title; ?>">
                            </figure>
                            <div>
                                <h2><?php echo $item->post_title; ?></h2>
                                <span class="date">
                                    <?php

                                    if($show_day) {
                                        echo $start_date . ' - ' . $end_date;
                                    }else{
                                        echo substr($start_date, 2) . ' - ' . substr($end_date, 2);
                                    }

                                    ?>
                                </span>
                                <?php echo $item->post_content; ?>
                                <a href="<?php echo $item->guid; ?>" class="more-exposition">
                                    <?php get_translation('read_more') ?>
                                </a>
                            </div>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div><!-- #content -->

    </div><!-- #container -->

<?php get_footer(); ?>