<?php
/**
 * Template name: Contact us
 */
?>

<?php
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

get_header(); ?>

    <iframe class="google-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2753.788831728331!2d7.129028715565176!3d46.35373528166777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x95f968bef222b8e0!2sMus%C3%A9e+des+Ormonts!5e0!3m2!1sfr!2sch!4v1556468301554!5m2!1sfr!2sch" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

    <div id="container" class="row-inner">
			<div id="content">

				<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
					<div class="entry-content clearfix">
						<?php the_content(); ?>
					</div><!-- .entry-content -->
				</article>

				<?php if (ot_get_option('page_comments') != 'off') {
					echo '<div class="row-inner"><div class="vc_span12 wpb_column column_container">';
						comments_template( '', true );
					echo '</div></div>';
				} ?>
				<?php endwhile; ?>

			</div><!-- #content -->
		</div><!-- #container -->
		
<?php get_footer(); ?>