<?php

global $post;

$poster = get_field('poster',$post->ID);
$banner_image = get_field('banner',$post->ID);
$gallery = get_field('gallerie_images',$post->ID);

$concour = get_field('concour',$post->ID);
$price_1 = $concour['1_price'];
$price_2 = $concour['2_price'];
$price_3 = $concour['3_price'];

$start_date = get_field('start_date_exposition', $post->ID);
$end_date = get_field('end_date_exposition', $post->ID);
$show_day = get_field('show_day', $post->ID);


$args = array(
    'post_type' => 'exposition',
    'orderby'   => 'rand',
    'posts_per_page' => 5,
);

$expositions = get_posts($args);

get_header('exposition'); ?>

    <figure class="banner">
        <img src="<?php echo $banner_image ?>" alt="">
    </figure>

    <div id="container" class="row-inner">

        <div id="content">

            <section class="exposition">

                <a href="<?php echo $poster ?>"  data-lightbox="poster">
                    <figure class="poster">
                        <img src="<?php echo $poster ?>" alt="">
                    </figure>
                </a>

                <h1><?php echo $post->post_title ?></h1>
                <span class="date">
                    <?php

                    if($show_day) {
                        echo $start_date . ' - ' . $end_date;
                    }else{
                        echo substr($start_date, 2) . ' - ' . substr($end_date, 2);
                    }

                    ?>
                </span>
                <p><?php echo $post->post_content ?></p>

                <?php if($gallery): ?>
                    <div class="gallery">
                        <h2><?php get_translation('expositions.gallery') ?></h2>
                        <?php foreach($gallery as $item): ?>
                            <a href="<?php echo $item['image'] ?>"  data-lightbox="exposition" data-title="<?php echo $item['label'] ?>">
                                <div style="background-image: url(<?php echo $item['image'] ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?php
                if($price_1 && $price_2 && $price_3) {
                    ?>
                    <div class="contest">
                        <h2><?php get_translation('expositions.contest') ?></h2>
                        <ul class="contest-list">
                            <li>
                                <a href="<?php echo $price_2['image'] ?>"  data-lightbox="concours" data-title="<?php echo $price_2['label'] ?>">
                                    <div style="background-image: url(<?php echo $price_2['image'] ?>">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                                    </div>
                                </a>
                                <p>
                                    <span><?php get_translation('expositions.2_price'); ?></span>
                                    <?php echo $price_2['label'] ?>
                                </p>
                            </li>
                            <li>
                                <a href="<?php echo $price_1['image'] ?>"  data-lightbox="concours" data-title="<?php echo $price_1['label'] ?>">
                                    <div style="background-image: url(<?php echo $price_1['image'] ?>">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                                    </div>
                                </a>
                                <p>
                                    <span><?php get_translation('expositions.1_price'); ?></span>
                                    <?php echo $price_1['label'] ?>
                                </p>
                            </li>
                            <li>
                                <a href="<?php $price_3['image'] ?>"  data-lightbox="concours" data-title="<?php echo $price_3['label'] ?>">
                                    <div style="background-image: url(<?php echo $price_3['image'] ?>">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                                    </div>
                                </a>
                                <p>
                                    <span><?php get_translation('expositions.3_price'); ?></span>
                                    <?php echo $price_3['label'] ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <?php
                }
                ?>

                <aside class="other-expo">
                    <h2><?php get_translation('expositions.other_expo') ?></h2>
                    <?php

                    foreach($expositions as $exposition){
                        if($exposition->ID != $post->ID) {

                            ?>
                            <a href="<?php echo $exposition->guid; ?>" title="<?php echo $exposition->post_title; ?>">
                                <article>
                                    <div style="background-image: url(<?php echo get_field('poster', $exposition->ID) ?>">
                                        <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                                    </div>
                                    <?php echo $exposition->post_title; ?>
                                </article>
                            </a>
                            <?php

                        }
                    }

                    ?>
                </aside>

            </section>

        </div><!-- #content -->

    </div><!-- #container -->

<?php get_footer('exposition'); ?>