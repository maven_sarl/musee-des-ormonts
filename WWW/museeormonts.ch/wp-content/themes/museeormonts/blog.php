<?php
/**
 * Template name: Blog
 */

$args = array(
    'post_type'         => 'post',
	'orderby'			=> 'date',
	'order'				=> 'DESC'
);

$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

$posts = get_posts( $args );

get_header('no-title'); ?>

    <?php if($featured_img_url): ?>
    <div class="banner-post" style="background-image: url(<?php echo $featured_img_url ?>)"></div>
    <?php endif; ?>

    <div id="container" class="row-inner">

            <div id="content">

                <h1><?php get_translation('blog.title') ?></h1>
                <?php foreach($posts as $item): ?>
                <?php
                    $image = get_the_post_thumbnail( $item->ID, 'full' );
                ?>
                    <article class="post">
                        <figure>
                            <?php echo $image ?>
                        </figure>
                        <div>
                            <h2><?php echo $item->post_title; ?></h2>
                            <p><?php echo $item->post_content; ?></p>
                            <a href="<?php echo $item->guid; ?>" class="more-exposition">
                                <?php get_translation('read_more') ?>
                            </a>
                        </div>
                    </article>
                <?php endforeach; ?>

            </div><!-- #content -->

    </div><!-- #container -->

<?php get_footer(); ?>