<article class="exposition">
    <figure>
        <img src="<?php echo $poster; ?>" alt="<?php echo $exposition->post_title; ?>">
    </figure>
    <div>
        <h2><?php echo $exposition->post_title; ?></h2>
        <span class="date">
            <?php

            if($show_day) {
                echo $start_date . ' - ' . $end_date;
            }else{
                echo substr($start_date, 2) . ' - ' . substr($end_date, 2);
            }

            ?>
        </span>
        <?php echo $exposition->post_content; ?>
        <a href="<?php echo $exposition->guid; ?>" class="more-exposition">
            <?php echo __('read more'); ?>
        </a>
    </div>
</article>