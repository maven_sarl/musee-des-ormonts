<?php
/**
 * Template name: Médias
 */

$medias = get_field('medias',$post->ID);

get_header(); ?>

		<div id="container" class="row-inner">
			<div id="content">
                <?php while ( have_posts() ) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
                        <div class="entry-content clearfix">
                            <?php the_content(); ?>
                        </div><!-- .entry-content -->
                    </article>
                    <?php if (ot_get_option('page_comments') != 'off') {
                        echo '<div class="row-inner"><div class="vc_span12 wpb_column column_container">';
                        comments_template( '', true );
                        echo '</div></div>';
                    } ?>
                <?php endwhile; ?>

                <section class="download-medias">
                    <h2><?php echo get_translation('download') ?></h2>
                    <table class="list-medias" cellpadding="0" cellspacing="0">
                        <tbody>
                        <?php foreach($medias as $media): ?>
                            <tr>
                                <td><?php echo $media['name'] ?></td>
                                <td><?php echo getFileSizeFromUrl($media['file_download']) ?></td>
                                <td><a href="<?php echo $media['file_download'] ?>"><?php echo do_shortcode('[vc_icon name="fa-download"]')?></a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </section>

			</div><!-- #content -->
		</div><!-- #container -->
		
<?php get_footer(); ?>