<?php

global $post;

$poster = get_field('poster',$post->ID);
$banner_image = get_field('banner',$post->ID);
$gallery = get_field('gallerie_images',$post->ID);

$start_date = get_field('start_date_exposition', $post->ID);
$end_date = get_field('end_date_exposition', $post->ID);
$show_day = get_field('show_day', $post->ID);


$args = array(
    'post_type' => 'post',
    'orderby'   => 'rand',
    'posts_per_page' => 5,
);

$banner_img_url = get_field('banner', $post->ID);
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

$posts = get_posts($args);

get_header('exposition'); ?>

    <a href="<?php echo $banner_img_url['url'] ?>"  data-lightbox="poster">
    <div class="banner-post" style="background-image: url(<?php echo $banner_img_url['url'] ?>)"></div>
    </a>

    <div id="container" class="row-inner">

        <div id="content">

            <section class="post">
                <figure class="photo-thumb">
                    <a href="<?php echo $featured_img_url ?>" data-lightbox="post-thumb">
                        <img src="<?php echo $featured_img_url ?>" />
                    </a>
                </figure>
                <h1><?php echo $post->post_title ?></h1>
                <span class="date">
                    <?php echo date("d F Y", strtotime($post->post_date)) ?>
                </span>
                <?php echo $post->post_content ?>

                <?php if($gallery): ?>
                    <div class="gallery">
                        <h2><?php get_translation('expositions.gallery') ?></h2>
                        <?php foreach($gallery as $item): ?>
                            <a href="<?php echo $item['image'] ?>"  data-lightbox="post-gallery" data-title="<?php echo $item['label'] ?>">
                                <div style="background-image: url(<?php echo $item['image'] ?>">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/blank.png" alt="" />
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?php if(sizeof($posts) > 1): ?>
                <aside class="other-expo">
                    <h2><?php get_translation('expositions.other_expo') ?></h2>
                    <?php

                    foreach($posts as $item){
                        if($item->ID != $post->ID) {

                            ?>

                            <article class="other-expo">
                                <a href="<?php echo $item->guid; ?>" title="<?php echo $item->post_title; ?>">
                                    <?php echo $item->post_title; ?>
                                </a>
                            </article>

                            <?php

                        }
                    }

                    ?>
                </aside>
                <?php endif; ?>

            </section>

        </div><!-- #content -->

    </div><!-- #container -->

<?php get_footer('exposition'); ?>